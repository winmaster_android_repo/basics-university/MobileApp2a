package katarzyna.mobileapp2a;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Toast.makeText(this.getApplicationContext(), "Hasta la vista baby",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        Toast.makeText(this.getApplicationContext(), "I'll be back!",Toast.LENGTH_SHORT).show();
    }

    public void openApp(View view)
    {
        String action="katarzyna.mobileapp2.AppActivity1";
        Intent intent=new Intent(action);
        startActivity(intent);
    }

    public void makePhoneCall(View view)
    {
        Intent intent= new Intent(Intent.ACTION_DIAL, Uri.parse("tel:662-859-099"));
        startActivity(intent);
    }

    public void sendSMS(View view)
    {
        Intent intent= new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:662859099"));
        intent.putExtra("sms_body", "SMS send by application, tell us your thoughts here: ");
        startActivity(intent);
    }

    public void searchNetwork(View view)
    {
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse("http://wiz.pwr.edu.pl"));
        startActivity(intent);
    }

    public void makePhoto(View view)
    {
        Intent intent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(intent);
    }
}
